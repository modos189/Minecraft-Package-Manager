# -*- coding: utf-8 -*-

import os, shutil

def mkdir(patch):
    if os.path.exists(patch)==False:
        os.mkdir(patch)

def move(a,b):
    shutil.move(a,b)

def term(command):
    if (os.name=='posix'):
        os.system(command)
    elif (os.name=='nt'):
    	os.system(command.encode('cp1251'))