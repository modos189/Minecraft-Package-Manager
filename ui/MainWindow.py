# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created: Wed Feb 20 13:49:13 2013
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(464, 493)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("icons/favicon.png")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.gridLayout = QtGui.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.pushButton_3 = QtGui.QPushButton(self.centralwidget)
        self.pushButton_3.setEnabled(True)
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))
        self.gridLayout.addWidget(self.pushButton_3, 2, 0, 1, 1)
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setEnabled(True)
        self.tabWidget.setBaseSize(QtCore.QSize(0, 0))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.gridLayout_2 = QtGui.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName(_fromUtf8("gridLayout_2"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.mc_progressBar = QtGui.QProgressBar(self.tab)
        self.mc_progressBar.setProperty("value", 24)
        self.mc_progressBar.setObjectName(_fromUtf8("mc_progressBar"))
        self.horizontalLayout.addWidget(self.mc_progressBar)
        self.mc_install = QtGui.QPushButton(self.tab)
        self.mc_install.setEnabled(False)
        self.mc_install.setObjectName(_fromUtf8("mc_install"))
        self.horizontalLayout.addWidget(self.mc_install)
        spacerItem = QtGui.QSpacerItem(37, 17, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.mc_more = QtGui.QPushButton(self.tab)
        self.mc_more.setEnabled(False)
        self.mc_more.setObjectName(_fromUtf8("mc_more"))
        self.horizontalLayout.addWidget(self.mc_more)
        self.gridLayout_2.addLayout(self.horizontalLayout, 1, 0, 1, 1)
        self.splitter = QtGui.QSplitter(self.tab)
        self.splitter.setOrientation(QtCore.Qt.Vertical)
        self.splitter.setObjectName(_fromUtf8("splitter"))
        self.listWidget = QtGui.QListWidget(self.splitter)
        self.listWidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.listWidget.setBaseSize(QtCore.QSize(0, 0))
        self.listWidget.setObjectName(_fromUtf8("listWidget"))
        self.textBrowser = QtGui.QTextBrowser(self.splitter)
        self.textBrowser.setObjectName(_fromUtf8("textBrowser"))
        self.gridLayout_2.addWidget(self.splitter, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setEnabled(True)
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.gridLayout_3 = QtGui.QGridLayout(self.tab_2)
        self.gridLayout_3.setObjectName(_fromUtf8("gridLayout_3"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.mod_progressBar = QtGui.QProgressBar(self.tab_2)
        self.mod_progressBar.setProperty("value", 24)
        self.mod_progressBar.setObjectName(_fromUtf8("mod_progressBar"))
        self.horizontalLayout_2.addWidget(self.mod_progressBar)
        self.mod_install = QtGui.QPushButton(self.tab_2)
        self.mod_install.setEnabled(False)
        self.mod_install.setObjectName(_fromUtf8("mod_install"))
        self.horizontalLayout_2.addWidget(self.mod_install)
        self.mod_remove = QtGui.QPushButton(self.tab_2)
        self.mod_remove.setEnabled(False)
        self.mod_remove.setObjectName(_fromUtf8("mod_remove"))
        self.horizontalLayout_2.addWidget(self.mod_remove)
        self.mod_disable = QtGui.QPushButton(self.tab_2)
        self.mod_disable.setEnabled(False)
        self.mod_disable.setObjectName(_fromUtf8("mod_disable"))
        self.horizontalLayout_2.addWidget(self.mod_disable)
        self.mod_enable = QtGui.QPushButton(self.tab_2)
        self.mod_enable.setEnabled(False)
        self.mod_enable.setObjectName(_fromUtf8("mod_enable"))
        self.horizontalLayout_2.addWidget(self.mod_enable)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.mod_more = QtGui.QPushButton(self.tab_2)
        self.mod_more.setEnabled(False)
        self.mod_more.setObjectName(_fromUtf8("mod_more"))
        self.horizontalLayout_2.addWidget(self.mod_more)
        self.gridLayout_3.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)
        self.splitter_2 = QtGui.QSplitter(self.tab_2)
        self.splitter_2.setOrientation(QtCore.Qt.Vertical)
        self.splitter_2.setObjectName(_fromUtf8("splitter_2"))
        self.listWidget_2 = QtGui.QListWidget(self.splitter_2)
        self.listWidget_2.setEnabled(True)
        self.listWidget_2.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.listWidget_2.setToolTip(_fromUtf8(""))
        self.listWidget_2.setAutoFillBackground(True)
        self.listWidget_2.setObjectName(_fromUtf8("listWidget_2"))
        self.layoutWidget = QtGui.QWidget(self.splitter_2)
        self.layoutWidget.setObjectName(_fromUtf8("layoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.textBrowser_2 = QtGui.QTextBrowser(self.layoutWidget)
        self.textBrowser_2.setObjectName(_fromUtf8("textBrowser_2"))
        self.verticalLayout.addWidget(self.textBrowser_2)
        self.textBrowser_3 = QtGui.QTextBrowser(self.layoutWidget)
        self.textBrowser_3.setMaximumSize(QtCore.QSize(16777215, 50))
        self.textBrowser_3.setObjectName(_fromUtf8("textBrowser_3"))
        self.verticalLayout.addWidget(self.textBrowser_3)
        self.gridLayout_3.addWidget(self.splitter_2, 0, 0, 1, 1)
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.gridLayout_4 = QtGui.QGridLayout(self.tab_3)
        self.gridLayout_4.setObjectName(_fromUtf8("gridLayout_4"))
        self.pushButton = QtGui.QPushButton(self.tab_3)
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.gridLayout_4.addWidget(self.pushButton, 1, 0, 1, 1)
        self.pushButton_2 = QtGui.QPushButton(self.tab_3)
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        self.gridLayout_4.addWidget(self.pushButton_2, 1, 2, 1, 1)
        self.listWidget_3 = QtGui.QListWidget(self.tab_3)
        self.listWidget_3.setObjectName(_fromUtf8("listWidget_3"))
        self.gridLayout_4.addWidget(self.listWidget_3, 0, 0, 1, 3)
        self.pushButton_4 = QtGui.QPushButton(self.tab_3)
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))
        self.gridLayout_4.addWidget(self.pushButton_4, 1, 1, 1, 1)
        self.tabWidget.addTab(self.tab_3, _fromUtf8(""))
        self.tab_4 = QtGui.QWidget()
        self.tab_4.setObjectName(_fromUtf8("tab_4"))
        self.gridLayout_5 = QtGui.QGridLayout(self.tab_4)
        self.gridLayout_5.setObjectName(_fromUtf8("gridLayout_5"))
        self.label_language = QtGui.QLabel(self.tab_4)
        self.label_language.setObjectName(_fromUtf8("label_language"))
        self.gridLayout_5.addWidget(self.label_language, 0, 0, 1, 4)
        self.comboBox_language = QtGui.QComboBox(self.tab_4)
        self.comboBox_language.setObjectName(_fromUtf8("comboBox_language"))
        self.gridLayout_5.addWidget(self.comboBox_language, 1, 0, 1, 3)
        self.pushButton_language = QtGui.QPushButton(self.tab_4)
        self.pushButton_language.setObjectName(_fromUtf8("pushButton_language"))
        self.gridLayout_5.addWidget(self.pushButton_language, 1, 3, 1, 1)
        self.line = QtGui.QFrame(self.tab_4)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.gridLayout_5.addWidget(self.line, 2, 0, 1, 4)
        self.label_select_server = QtGui.QLabel(self.tab_4)
        self.label_select_server.setObjectName(_fromUtf8("label_select_server"))
        self.gridLayout_5.addWidget(self.label_select_server, 3, 0, 1, 4)
        self.pushButton_reset_select_server = QtGui.QPushButton(self.tab_4)
        self.pushButton_reset_select_server.setObjectName(_fromUtf8("pushButton_reset_select_server"))
        self.gridLayout_5.addWidget(self.pushButton_reset_select_server, 5, 0, 1, 1)
        spacerItem2 = QtGui.QSpacerItem(311, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout_5.addItem(spacerItem2, 5, 1, 1, 2)
        self.pushButton_apply_select_server = QtGui.QPushButton(self.tab_4)
        self.pushButton_apply_select_server.setObjectName(_fromUtf8("pushButton_apply_select_server"))
        self.gridLayout_5.addWidget(self.pushButton_apply_select_server, 5, 3, 1, 1)
        spacerItem3 = QtGui.QSpacerItem(20, 297, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout_5.addItem(spacerItem3, 6, 2, 1, 1)
        self.lineEdit = QtGui.QLineEdit(self.tab_4)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.gridLayout_5.addWidget(self.lineEdit, 4, 0, 1, 4)
        self.tabWidget.addTab(self.tab_4, _fromUtf8(""))
        self.gridLayout.addWidget(self.tabWidget, 1, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 464, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.toolBar = QtGui.QToolBar(MainWindow)
        self.toolBar.setMinimumSize(QtCore.QSize(0, 0))
        self.toolBar.setObjectName(_fromUtf8("toolBar"))
        MainWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        self.action_start = QtGui.QAction(MainWindow)
        self.action_start.setObjectName(_fromUtf8("action_start"))
        self.action_copy = QtGui.QAction(MainWindow)
        self.action_copy.setObjectName(_fromUtf8("action_copy"))
        self.action_about = QtGui.QAction(MainWindow)
        self.action_about.setObjectName(_fromUtf8("action_about"))
        self.action_donate = QtGui.QAction(MainWindow)
        self.action_donate.setObjectName(_fromUtf8("action_donate"))
        self.toolBar.addAction(self.action_start)
        self.toolBar.addAction(self.action_copy)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.action_about)
        self.toolBar.addAction(self.action_donate)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.tabWidget, self.listWidget)
        MainWindow.setTabOrder(self.listWidget, self.textBrowser)
        MainWindow.setTabOrder(self.textBrowser, self.mc_install)
        MainWindow.setTabOrder(self.mc_install, self.mc_more)
        MainWindow.setTabOrder(self.mc_more, self.pushButton_3)
        MainWindow.setTabOrder(self.pushButton_3, self.listWidget_2)
        MainWindow.setTabOrder(self.listWidget_2, self.textBrowser_2)
        MainWindow.setTabOrder(self.textBrowser_2, self.mod_install)
        MainWindow.setTabOrder(self.mod_install, self.mod_remove)
        MainWindow.setTabOrder(self.mod_remove, self.mod_disable)
        MainWindow.setTabOrder(self.mod_disable, self.mod_enable)
        MainWindow.setTabOrder(self.mod_enable, self.mod_more)
        MainWindow.setTabOrder(self.mod_more, self.listWidget_3)
        MainWindow.setTabOrder(self.listWidget_3, self.pushButton)
        MainWindow.setTabOrder(self.pushButton, self.pushButton_4)
        MainWindow.setTabOrder(self.pushButton_4, self.pushButton_2)
        MainWindow.setTabOrder(self.pushButton_2, self.comboBox_language)
        MainWindow.setTabOrder(self.comboBox_language, self.pushButton_language)
        MainWindow.setTabOrder(self.pushButton_language, self.lineEdit)
        MainWindow.setTabOrder(self.lineEdit, self.pushButton_reset_select_server)
        MainWindow.setTabOrder(self.pushButton_reset_select_server, self.pushButton_apply_select_server)
        MainWindow.setTabOrder(self.pushButton_apply_select_server, self.textBrowser_3)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "Minecraft Package Manager", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_3.setText(QtGui.QApplication.translate("MainWindow", "Add the file to the database", None, QtGui.QApplication.UnicodeUTF8))
        self.mc_install.setText(QtGui.QApplication.translate("MainWindow", "Install", None, QtGui.QApplication.UnicodeUTF8))
        self.mc_more.setText(QtGui.QApplication.translate("MainWindow", "More", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QtGui.QApplication.translate("MainWindow", "Minecraft", None, QtGui.QApplication.UnicodeUTF8))
        self.mod_install.setText(QtGui.QApplication.translate("MainWindow", "Install", None, QtGui.QApplication.UnicodeUTF8))
        self.mod_remove.setText(QtGui.QApplication.translate("MainWindow", "Remove", None, QtGui.QApplication.UnicodeUTF8))
        self.mod_disable.setText(QtGui.QApplication.translate("MainWindow", "Disable", None, QtGui.QApplication.UnicodeUTF8))
        self.mod_enable.setText(QtGui.QApplication.translate("MainWindow", "Enable", None, QtGui.QApplication.UnicodeUTF8))
        self.mod_more.setText(QtGui.QApplication.translate("MainWindow", "More", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QtGui.QApplication.translate("MainWindow", "Mods", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtGui.QApplication.translate("MainWindow", "Restore", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_2.setText(QtGui.QApplication.translate("MainWindow", "Create backup", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_4.setText(QtGui.QApplication.translate("MainWindow", "Remove", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QtGui.QApplication.translate("MainWindow", "Backups", None, QtGui.QApplication.UnicodeUTF8))
        self.label_language.setText(QtGui.QApplication.translate("MainWindow", "Language", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_language.setText(QtGui.QApplication.translate("MainWindow", "Apply", None, QtGui.QApplication.UnicodeUTF8))
        self.label_select_server.setText(QtGui.QApplication.translate("MainWindow", "This feature provides a way to obtain files from third-party servers", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_reset_select_server.setText(QtGui.QApplication.translate("MainWindow", "Reset", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_apply_select_server.setText(QtGui.QApplication.translate("MainWindow", "Apply", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), QtGui.QApplication.translate("MainWindow", "Settings", None, QtGui.QApplication.UnicodeUTF8))
        self.toolBar.setWindowTitle(QtGui.QApplication.translate("MainWindow", "toolBar", None, QtGui.QApplication.UnicodeUTF8))
        self.action_start.setText(QtGui.QApplication.translate("MainWindow", "Start game", None, QtGui.QApplication.UnicodeUTF8))
        self.action_copy.setText(QtGui.QApplication.translate("MainWindow", "Copy Launcher on your desktop", None, QtGui.QApplication.UnicodeUTF8))
        self.action_about.setText(QtGui.QApplication.translate("MainWindow", "About MPM", None, QtGui.QApplication.UnicodeUTF8))
        self.action_donate.setText(QtGui.QApplication.translate("MainWindow", "Donate", None, QtGui.QApplication.UnicodeUTF8))

