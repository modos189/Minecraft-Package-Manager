# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'user_hello.ui'
#
# Created: Wed Jan  2 12:26:11 2013
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_user_hello(object):
    def setupUi(self, user_hello):
        user_hello.setObjectName(_fromUtf8("user_hello"))
        user_hello.resize(412, 202)
        self.centralwidget = QtGui.QWidget(user_hello)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 391, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.frame = QtGui.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 50, 391, 51))
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(10, 10, 151, 31))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.comboBox = QtGui.QComboBox(self.frame)
        self.comboBox.setGeometry(QtCore.QRect(180, 10, 151, 31))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.pushButton = QtGui.QPushButton(self.frame)
        self.pushButton.setGeometry(QtCore.QRect(340, 10, 41, 31))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.frame_2 = QtGui.QFrame(self.centralwidget)
        self.frame_2.setGeometry(QtCore.QRect(10, 110, 391, 81))
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.label_3 = QtGui.QLabel(self.frame_2)
        self.label_3.setGeometry(QtCore.QRect(10, 10, 371, 31))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.pushButton_2 = QtGui.QPushButton(self.frame_2)
        self.pushButton_2.setGeometry(QtCore.QRect(280, 40, 101, 31))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))
        user_hello.setCentralWidget(self.centralwidget)

        self.retranslateUi(user_hello)
        QtCore.QMetaObject.connectSlotsByName(user_hello)

    def retranslateUi(self, user_hello):
        user_hello.setWindowTitle(QtGui.QApplication.translate("user_hello", "Warning!", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("user_hello", "You have already installed the minecraft. Select an action:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("user_hello", "1. Select which version<br>of the game is installed:", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton.setText(QtGui.QApplication.translate("user_hello", "OK", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("user_hello", "2. Continue and install another<br>version of the game by using MPM", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButton_2.setText(QtGui.QApplication.translate("user_hello", "Continue", None, QtGui.QApplication.UnicodeUTF8))

