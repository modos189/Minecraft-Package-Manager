#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
reload(sys)
sys.setdefaultencoding('utf-8')
from datetime import *
import time

if (os.name=='posix'):
    s = u'/'
    pth = os.path.expanduser('~')+s
elif (os.name=='nt'):
    s = u'\\'
    pth = unicode(os.path.expanduser('~').decode('cp1251'))+s+'Application Data'+s
else:
    print 'unknown platform'
    quit()

from patterns import *

mkdir(pth+'.mpm_cache')
mkdir(pth+'.mpm_cache'+s+'minecraft')
mkdir(pth+'.mpm_cache'+s+'mods')
sys.stdout = open(pth+'.mpm_cache'+s+"log.txt","a+")
sys.stderr = open(pth+'.mpm_cache'+s+"log.txt","a+")
print '\n\n\n'+str(datetime.now())[:-7]+'  MPM started'

import ConfigParser
import urllib
import getpass
import shutil
import webbrowser
import urllib2
import re
import hashlib

from PyQt4 import QtCore, QtGui
# import windows
from ui.MainWindow import Ui_MainWindow
from ui.user_hello import Ui_user_hello
from ui.about import Ui_about

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

default_server = "http://mpm.modostroi.ru/"

global dtm
dtm = u'.minecraft'

mkdir(pth+dtm)
mkdir(pth+dtm+s+'backup')

if os.path.dirname(os.path.realpath(__file__))!='':
    if (os.name=='nt'):
        ich=unicode(os.path.dirname(os.path.realpath(__file__).decode('cp1251'))+s)
    else:
        ich=os.path.dirname(os.path.realpath(__file__))+s
else:
    ich=''

program_name = 'Minecraft Package Manager'
program_version = '2.1'

class MainWindow(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self)
        self.setupUi(self)

        self.mc_progressBar.hide()
        self.mod_progressBar.hide()
        self.mod_remove.hide()
        self.mod_enable.hide()
        self.mod_disable.hide()

        self.setWindowIcon(QtGui.QIcon(ich+"icons/icon_16.png"))

        self.initializing()
        self.unpacking_client()

        self.listWidget.clicked.connect(self.minecraft_desc)
        self.listWidget_2.clicked.connect(self.mod_desc)

        self.mc_install.clicked.connect(self.select_minecraft_version)

        self.mod_install.clicked.connect(self.select_mod)
        self.mod_enable.clicked.connect(self.select_mod_on_off)
        self.mod_remove.clicked.connect(self.select_mod_del)

        self.pushButton_3.clicked.connect(self.addmod)

        self.pushButton_language.clicked.connect(self.select_language)

        self.mc_more.clicked.connect(self.link_more_mc)
        self.mod_more.clicked.connect(self.link_more_mod)

        self.action_start.triggered.connect(self.start)
        self.action_copy.triggered.connect(self.copy_launcher)
        self.action_about.triggered.connect(self.about)
        self.action_donate.triggered.connect(self.donate)

        self.search_of_updating()

        self.backups_list()

        self.active_server()

        self.find_language()

        if (QtGui.QIcon.hasThemeIcon("media-playback-start")):
            self.action_start.setIcon(QtGui.QIcon.fromTheme("media-playback-start"))
        else:
            self.action_start.setIcon(QtGui.QIcon(ich+"icons/media-playback-start.png"))

        if (QtGui.QIcon.hasThemeIcon("edit-copy")):
            self.action_copy.setIcon(QtGui.QIcon.fromTheme("edit-copy"))
        else:
            self.action_copy.setIcon(QtGui.QIcon(ich+"icons/edit-copy.png"))

        if (QtGui.QIcon.hasThemeIcon("info")):
            self.action_about.setIcon(QtGui.QIcon.fromTheme("info"))
        else:
            self.action_about.setIcon(QtGui.QIcon(ich+"icons/info.png"))

        if (QtGui.QIcon.hasThemeIcon("help-donate")):
            self.action_donate.setIcon(QtGui.QIcon.fromTheme("help-donate"))
        else:
            self.action_donate.setIcon(QtGui.QIcon(ich+"icons/donate.png"))

    def backups_list(self):
        self.listWidget_3.clear()
        dir_backups = os.listdir(pth+dtm+s+'backup'+s)
        for line in dir_backups:
            QtGui.QListWidgetItem(self.listWidget_3)

        self.pushButton.clicked.connect(self.backup_restore)
        self.pushButton_4.clicked.connect(self.backup_remove)
        self.pushButton_2.clicked.connect(self.backup_create)

        dir_backups = os.listdir(pth+dtm+s+'backup'+s)
        i=0
        for line in dir_backups:
            self.listWidget_3.item(i).setText(
                QtGui.QApplication.translate("ControlBackup", line,
                None, QtGui.QApplication.UnicodeUTF8))
            i+=1

    def search_of_updating(self):
        p = urllib2.urlopen(default_server+'last_version.txt')
        last_version = p.read()
        if (last_version[0] >= program_version[0] and
            last_version[2] > program_version[2]):
            self.label = QtGui.QLabel(self.centralwidget)
            self.label.setObjectName("label")
            self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
            self.label.setAlignment(QtCore.Qt.AlignCenter)
            self.label.setText('<a href="'+default_server+'">'+
                QtGui.QApplication.translate("MainWindow",
                "Software update available: v", None,
                QtGui.QApplication.UnicodeUTF8)+str(last_version)+'</a>')
            print last_version
            QtCore.QCoreApplication.processEvents()

            self.label.linkActivated.connect(self.uuu)

    def uuu(self):
        webbrowser.open_new_tab('http://modostroi.ru/forum/view_theme/130')

    def donate(self):
        webbrowser.open_new_tab('http://mpm.modostroi.ru/?donate')

    def updateListMods(self):
        self.listWidget_2.clear()
        if minecraft_version!='':
            self.tab_2.setEnabled(True)
            try:
                mods_list_package = ConfigParser.ConfigParser()
                mods_list_package.read(pth+'.mpm_cache'+s+mods_list_file)
                self.listWidget_2.clear()
                i=0
                for line in mods_list_package.sections():
                    QtGui.QListWidgetItem(self.listWidget_2)
                    self.listWidget_2.item(i).setText(
                        QtGui.QApplication.translate("MainWindow", line,
                        None, QtGui.QApplication.UnicodeUTF8))
                    if (mods_list_package.has_option(line, 'in_mods') or mods_list_package.has_option(line, 'in_jar')):
                        print line

                        if (mods_list_package.has_option(line, 'in_mods')):
                            if (mods_list_package.get(line, 'in_mods')=='<THIS>'):
                                in_mods = mods_list_package.get(line, 'filename')
                            else:
                                in_mods = re.sub('.*?\/', '', mods_list_package.get(line, 'in_mods'))
                            print in_mods
                        
                        if ((mods_list_package.has_option(line, 'in_mods') and
                            in_mods!='' and os.path.exists(pth+dtm+s+'mods'+s+in_mods)) or
                            (mods_list_package.has_option(line, 'mc_loader') and
                            os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mods_list_package.get(line, 'mc_loader')))):

                            icon1 = QtGui.QIcon()
                            icon1.addPixmap(QtGui.QPixmap(ich+"icons"+s+"tick.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                            self.listWidget_2.item(i).setIcon(icon1)

                        elif ((mods_list_package.has_option(line, 'in_mods') and
                            os.path.exists(pth+dtm+s+'mods'+s+in_mods+'.off')) or
                            (mods_list_package.has_option(line, 'mc_loader') and
                            os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mods_list_package.get(line, 'mc_loader')+'.off'))):

                            icon1 = QtGui.QIcon()
                            icon1.addPixmap(QtGui.QPixmap(ich+"icons"+s+"cross.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                            self.listWidget_2.item(i).setIcon(icon1)

                    i+=1
            except:
                pass

    def updateListClients(self):
        self.listWidget.clear()
        menecraft_list_package = ConfigParser.ConfigParser()
        menecraft_list_package.read(pth+'.mpm_cache'+s+'menecraft_info_package.ini')
        i=0
        for line in menecraft_list_package.sections():
            QtGui.QListWidgetItem(self.listWidget)
            self.listWidget.item(i).setText(
                QtGui.QApplication.translate("MainWindow", line,
                None, QtGui.QApplication.UnicodeUTF8))
            i+=1

    def addmod(self):
        webbrowser.open_new_tab('http://modostroi.ru/loads/add_mod_form/115')

    def link_more_mc(self):
        for item in self.listWidget.selectedItems():
            select_version = item.text()
        conf_info = ConfigParser.ConfigParser()
        conf_info.read(pth+'.mpm_cache'+s+'menecraft_info_package.ini')
        if (conf_info.get(str(select_version), 'more')):
            webbrowser.open_new_tab(conf_info.get(str(select_version), 'more'))

    def link_more_mod(self):
        for item in self.listWidget_2.selectedItems():
            select_version = item.text()
        conf_info = ConfigParser.ConfigParser()
        conf_info.read(pth+'.mpm_cache'+s+mods_list_file)
        if (conf_info.get(str(select_version), 'more')):
            webbrowser.open_new_tab(conf_info.get(str(select_version), 'more'))

    def select_mod_del(self):
        for item in self.listWidget_2.selectedItems():
            select_mod = str(item.text())
        mod_info = ConfigParser.ConfigParser()
        mod_info.read(pth+'.mpm_cache'+s+mods_list_file)

        if (mod_info.has_option(select_mod, 'in_mods')):
            if (mod_info.get(select_mod, 'in_mods')=='<THIS>'):
                in_mods = mod_info.get(select_mod, 'filename')
            else:
                in_mods = re.sub('.*?\/', '', mod_info.get(select_mod, 'in_mods'))
            remove(pth+dtm+s+'mods'+s+in_mods)
            remove(pth+dtm+s+'mods'+s+in_mods+'.off')
        if mod_info.has_option(select_mod, 'mc_loader'):
            remove(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mc_loader)
            remove(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mc_loader+'.off')

        self.pushButton_mod_del.setEnabled(False)
        self.updateListMods()

    def select_mod_on_off(self):
        for item in self.listWidget_2.selectedItems():
            select_mod = str(item.text())
        mod_info = ConfigParser.ConfigParser()
        mod_info.read(pth+'.mpm_cache'+s+mods_list_file)

        if (mod_info.has_option(select_mod, 'mc_loader') and
            os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader'))):
            move(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader'),pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader')+'.off')
        elif (mod_info.has_option(select_mod, 'mc_loader') and
            os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader')+'.off')):
            move(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader')+'.off',pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader'))

        if (mod_info.has_option(select_mod, 'in_mods')):
            if (mod_info.get(select_mod, 'in_mods')=='<THIS>'):
                in_mods = mod_info.get(select_mod, 'filename')
            else:
                in_mods = re.sub('.*?\/', '', mod_info.get(select_mod, 'in_mods'))
            if (os.path.exists(pth+dtm+s+'mods'+s+in_mods)):
                move(pth+dtm+s+'mods'+s+in_mods,pth+dtm+s+'mods'+s+in_mods+'.off')
            elif (os.path.exists(pth+dtm+s+'mods'+s+in_mods+'.off')):
                move(pth+dtm+s+'mods'+s+in_mods+'.off',pth+dtm+s+'mods'+s+in_mods)

        self.updateListMods()

    def active_server(self):
        self.lineEdit.setText(server)

        self.pushButton_reset_select_server.clicked.connect(self.active_server_default)

        self.pushButton_apply_select_server.clicked.connect(self.active_server_set)

    def active_server_default(self):
        self.lineEdit.setText(default_server)

    def active_server_set(self):
        global server
        server = str(self.lineEdit.text())

        config = ConfigParser.ConfigParser()
        config.read(pth+'.mpm_cache'+s+'config.ini')

        if (config.has_section("mpm")==False):
            config.add_section("mpm")
        config.set("mpm", "server", server)

        with open(pth+'.mpm_cache'+s+'config.ini', 'wb') as configfile:
            config.write(configfile)

        self.initializing()

    # I will be very thankful, if you optimize a code below
    def find_language(self):
        self.comboBox_language.clear()
        list_language = []
        list_language += [QtGui.QApplication.translate("Languages", "Use system default", None, QtGui.QApplication.UnicodeUTF8)]
        list_language += [QtGui.QApplication.translate(
            "Languages", "English (%s)" % (QtGui.QApplication.translate(
            "Languages", "Original", None, QtGui.QApplication.UnicodeUTF8)),
            None, QtGui.QApplication.UnicodeUTF8)]
        if (os.path.isdir(ich+s+'locals')):
            for line in os.listdir(ich+'locals'):
                if (line[-3:]=='.qm'):
                    line = line[:-3]
                    line = line.replace('ru_RU', 'Russian - Русский')
                    list_language += [line]

        i=0
        for line in list_language:
            self.comboBox_language.addItem(_fromUtf8(""))
            self.comboBox_language.setItemText(i, QtGui.QApplication.translate(
                "MainWindow", line, None, QtGui.QApplication.UnicodeUTF8))
            i+=1
        self.active_language()


    def active_language(self):
        config = ConfigParser.ConfigParser()
        config.read(pth+'.mpm_cache'+s+'config.ini')

        if (config.has_option('mpm', 'locale')==True):
            locale = config.get('mpm', 'locale')
        else:
            locale = 'default'

        if locale=='default':
            self.comboBox_language.setCurrentIndex(0)
        elif locale=='STANDARD':
            self.comboBox_language.setCurrentIndex(1)
        else:
            i=2
            if (os.path.isdir(ich+s+'locals')):
                for line in os.listdir(ich+'locals'):
                    if (line[-3:]=='.qm'):
                        if (locale == line[:-3]):
                            self.comboBox_language.setCurrentIndex(i)
                        i+=1

    def select_language(self):
        id_language = self.comboBox_language.currentIndex()
        if (id_language == 0):
            select_language = 'default'
        elif (id_language == 1):
            select_language = 'STANDARD'
        else:
            i=2
            if (os.path.isdir(ich+s+'locals')):
                    for line in os.listdir(ich+'locals'):
                        if (line[-3:]=='.qm'):
                            if (id_language == i):
                                select_language = line[:-3]
                            i+=1

        config = ConfigParser.ConfigParser()
        config.read(pth+'.mpm_cache'+s+'config.ini')

        if (config.has_section("mpm")==False):
            config.add_section("mpm")
        config.set("mpm", "locale", select_language)

        with open(pth+'.mpm_cache'+s+'config.ini', 'wb') as configfile:
            config.write(configfile)

        QtGui.QMessageBox.information(QtGui.QWidget(), _fromUtf8("Restart"),
            _fromUtf8("You must restart the application"), QtGui.QMessageBox.Ok)

    def about(self):
        second_window = About(parent=self)
        second_window.show()

    ############################################################################
    # < launcher >
    ############################################################################
    def start(self):
        term('java -jar \"'+ich+'bin"'+s+'launcher.jar '+dtm)

    def copy_launcher(self):
        if (os.name=='posix'):
            s = u'/'
            ptha = pth
        elif (os.name=='nt'):
            s = u'\\'
            ptha = unicode(os.path.expanduser('~').decode('cp1251'))+s

        print ptha+'Рабочий стол'
        if (os.path.isdir(ptha+'Desktop')):
            desktop_folder = 'Desktop'
        elif (os.path.isdir(ptha+'Рабочий стол')):
            desktop_folder = 'Рабочий стол'
        else:
            print 'folder not found'
            return

        if (os.name=='posix'):
            term('cp \''+ich+'\'bin/launcher.jar '+
                ptha+'\''+desktop_folder+'\'/Minecraft.jar')
        elif (os.name=='nt'):
            term('copy /Y \"'+ich+'\"bin\launcher.jar \"'+ptha+desktop_folder+'\"\Minecraft.jar')
    ############################################################################
    # </ launcher >
    ############################################################################







    ############################################################################
    # < initializing program >
    ############################################################################
    def initializing(self):
        global server
        server = default_server
        error = QtGui.QApplication.translate("Initializing",
                "You must install Minecraft of MPM", None,
                QtGui.QApplication.UnicodeUTF8)
        self.loliminecraft()
        self.updateListClients()

        self.tab_2.setEnabled(False)
        if os.path.exists(pth+'.mpm_cache'+s+'config.ini'):
            config = ConfigParser.ConfigParser()
            config.read(pth+'.mpm_cache'+s+'config.ini')
            if (config.has_option('mpm', 'dtm')==True):
                global dtm
                dtm = config.get('mpm', 'dtm')

            if (config.has_option(dtm, 'version')==True):
                global minecraft_version
                minecraft_version = config.get(dtm, 'version')
                self.tab_2.setEnabled(True)

                global mods_list_file
                mods_list_file = 'mods_'+minecraft_version+'.ini'

                config = ConfigParser.ConfigParser()
                config.read(pth+'.mpm_cache'+s+'config.ini')
                if (config.has_option('mpm', 'server')):
                    server = config.get('mpm', 'server')

                self.statusbar.showMessage(QtGui.QApplication.translate(
                    "Initializing", "Currently installed version of minecraft", None,
                    QtGui.QApplication.UnicodeUTF8)+' '+minecraft_version)

                self.lolimods()
                self.updateListMods()
            else:
                if os.path.exists(pth+dtm+s+u'bin'+s+'minecraft.jar'):
                    self.statusbar.showMessage(error)

                    second_window = User_hello(parent=self)
                    second_window.show()
        else:
            create_congig_txt = open(pth+'.mpm_cache'+s+'config.ini', 'w')
            create_congig_txt.close()
            if os.path.exists(pth+dtm+s+u'bin'):
                self.statusbar.showMessage(error)

            if (os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar')):
                second_window = User_hello(parent=self)
                second_window.show()

    # load lists
    def loliminecraft(self):
        try:
            urllib.urlretrieve(server+'menecraft_info_package.ini',
                filename=pth+'.mpm_cache'+s+'menecraft_info_package.ini')
        except:
            self.statusbar.showMessage(QtGui.QApplication.translate(
                        "load lists", "list package minecraft not available", None,
                        QtGui.QApplication.UnicodeUTF8))
        try:
            mods_list_package = ConfigParser.ConfigParser()
            mods_list_package.read(pth+'.mpm_cache'+s+'menecraft_info_package.ini')
        except:
            remove(pth+'.mpm_cache'+s+'menecraft_info_package.ini')
            self.statusbar.showMessage(QtGui.QApplication.translate(
                        "load lists", "list package minecraft damaged", None,
                        QtGui.QApplication.UnicodeUTF8))

    def lolimods(self):
        try:
            urllib.urlretrieve(server+mods_list_file,
                filename=pth+'.mpm_cache'+s+mods_list_file)
        except:
            self.statusbar.showMessage(QtGui.QApplication.translate(
                        "load lists", "list package mods not available", None,
                        QtGui.QApplication.UnicodeUTF8))
        try:
            mods_list_package = ConfigParser.ConfigParser()
            mods_list_package.read(pth+'.mpm_cache'+s+mods_list_file)
        except:
            remove(str(pth+'.mpm_cache'+s+mods_list_file))
            self.statusbar.showMessage(QtGui.QApplication.translate(
                        "load lists", "list package mods damaged", None,
                        QtGui.QApplication.UnicodeUTF8))
    ############################################################################
    # </ initializing program >
    ############################################################################

    def unpacking_client(self):
        if (os.path.isfile(pth+dtm+s+u'bin'+s+'minecraft.jar')):
            pth_to_bin = pth+dtm+s+'bin'

            if (os.name=='posix'):
                term('cd '+pth_to_bin+' && unzip minecraft.jar -d minecraft_jar')
            elif (os.name=='nt'):
                term('bin\\7za.exe x "'+pth_to_bin+'\\minecraft.jar" -o"'+pth_to_bin+'\\minecraft_jar"')

            remove(pth_to_bin+s+'minecraft.jar')

            move(pth_to_bin+s+'minecraft_jar', pth_to_bin+s+'minecraft.jar')



    ############################################################################
    # < desc on click >
    ############################################################################
    def minecraft_desc(self, QModelIndex):
        for item in self.listWidget.selectedItems():
            select_version = str(item.text())

        self.mc_install.show()
        self.mc_install.setEnabled(True)
        self.mc_more.setEnabled(True)
        self.mc_progressBar.hide()

        menecraft_info_package = ConfigParser.ConfigParser()
        menecraft_info_package.read(pth+'.mpm_cache'+s+'menecraft_info_package.ini')

        desc = menecraft_info_package.get(select_version, 'desc')
        
        self.textBrowser.setHtml(QtGui.QApplication.translate(
                "MainWindow", desc, None, QtGui.QApplication.UnicodeUTF8))

    def mod_desc(self, QModelIndex):
        for item in self.listWidget_2.selectedItems():
            select_mod = str(item.text())

        self.mod_install.show()
        self.mod_install.setEnabled(True)
        self.mod_more.setEnabled(True)
        self.mod_progressBar.hide()
        self.mod_remove.hide()
        self.mod_disable.hide()
        self.mod_enable.hide()

        mod_info = ConfigParser.ConfigParser()
        mod_info.read(pth+'.mpm_cache'+s+mods_list_file)

        desc = mod_info.get(select_mod, 'desc')
        if mod_info.get(select_mod, 'depending')!='':
            depending = mod_info.get(select_mod, 'depending')
        else:
            depending = QtGui.QApplication.translate("mod_desc", "no", None, QtGui.QApplication.UnicodeUTF8)

        self.textBrowser_2.setHtml(QtGui.QApplication.translate(
            "mod_desc", desc, None, QtGui.QApplication.UnicodeUTF8))

        self.textBrowser_3.setHtml(QtGui.QApplication.translate(
            "mod_desc", 'Depending:', None,
            QtGui.QApplication.UnicodeUTF8)+'<br>'+depending)

        if (mod_info.has_option(select_mod, 'in_mods') or
            mod_info.has_option(select_mod, 'mc_loader')):
                print select_mod

                if (mod_info.has_option(select_mod, 'in_mods')):
                    if (mod_info.get(select_mod, 'in_mods')=='<THIS>'):
                        in_mods = mod_info.get(select_mod, 'filename')
                    else:
                        in_mods = re.sub('.*?\/', '', mod_info.get(select_mod, 'in_mods'))

                self.mod_remove.setEnabled(True)
                self.mod_disable.setEnabled(True)
                self.mod_enable.setEnabled(True)
                self.mod_remove.show()
                if ((mod_info.has_option(select_mod, 'mc_loader') and
                    os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader'))) or
                    (mod_info.has_option(select_mod, 'in_mods') and in_mods!='' and
                    os.path.exists(pth+dtm+s+'mods'+s+in_mods))):

                    self.mod_disable.setText(QtGui.QApplication.translate("mod_desc", "Disable", None, QtGui.QApplication.UnicodeUTF8))
                    self.mod_disable.show()

                elif ((mod_info.has_option(select_mod, 'mc_loader') and
                    os.path.exists(pth+dtm+s+'bin'+s+'minecraft.jar'+s+mod_info.get(select_mod, 'mc_loader')+'.off')) or
                    (mod_info.has_option(select_mod, 'in_mods') and in_mods!='' and
                    os.path.exists(pth+dtm+s+'mods'+s+in_mods+'.off'))):

                    self.mod_enable.setText(QtGui.QApplication.translate("mod_desc", "Enable", None, QtGui.QApplication.UnicodeUTF8))
                    self.mod_enable.show()
                    self.mod_remove.show()
                else:
                    self.mod_remove.setEnabled(False)
                    self.mod_disable.setEnabled(False)
                    self.mod_enable.setEnabled(False)
                    self.mod_remove.hide()

    ############################################################################
    # </ desc on click >
    ############################################################################



    ############################################################################
    # < install minecraft >
    ############################################################################
    def loadProgressClient(self, bl, blsize, size):
        p = float(min(bl*blsize, size)) / size * 85 + 5
        self.mc_progressBar.setProperty("value", p)
        self.mod_progressBar.setProperty("value", p)
        QtCore.QCoreApplication.processEvents()

    def select_minecraft_version(self):
        for item in self.listWidget.selectedItems():
            select_version = str(item.text())

        self.mc_progressBar.show()
        self.mc_install.hide()

        menecraft_info_package = ConfigParser.ConfigParser()
        menecraft_info_package.read(pth+'.mpm_cache'+s+'menecraft_info_package.ini')
        url = menecraft_info_package.get(select_version, 'url')
        filename = menecraft_info_package.get(select_version, 'filename')
        md5 = menecraft_info_package.get(select_version, 'md5')

        set_status = '<style>green {color: green;}</style>'
        ok = ' <green>'+QtGui.QApplication.translate("progress_install_minecraft", "ready", None, QtGui.QApplication.UnicodeUTF8)+'</green><br>'
        set_status += QtGui.QApplication.translate("progress_install_minecraft", "Pre-installation preparation", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser.setHtml(set_status)
        self.mc_progressBar.setProperty("value", 1)

        set_status += ok+QtGui.QApplication.translate("progress_install_minecraft", "Download client", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser.setHtml(set_status)
        
        ptf = pth+'.mpm_cache'+s+'minecraft'+s+filename
        if (os.path.exists(ptf)==True and md5hash(ptf)==md5):
            pass
        else:
            urllib.urlretrieve(url, ptf, self.loadProgressClient)

        set_status += ok+QtGui.QApplication.translate("progress_install_minecraft", "Unpacking the archive", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser.setHtml(set_status)

        self.mc_progressBar.setProperty("value", 91)

        print filename

        qazwsx = filename.split('.')

        # clear dir
        removedir(pth+'.mpm_cache'+s+'minecraft'+s+'temp')
        mkdir(pth+'.mpm_cache'+s+'minecraft'+s+'temp')

        if (os.name=='posix'):
            if (qazwsx[1]=='zip'):
                term('cd '+pth+'.mpm_cache/minecraft/ && unzip '+filename+' -d temp')
            elif (qazwsx[1]=='7z'):
                term('cd '+pth+'.mpm_cache/minecraft/ && 7z x '+filename+' -otemp')
            else:
                print qazwsx[1]+' - unknown format'
        elif (os.name=='nt'):
            term('bin\\7za.exe x "'+pth+'.mpm_cache\\minecraft\\'+filename+'" -o"'+pth+'.mpm_cache\\minecraft\\temp"')

        self.mc_progressBar.setProperty("value", 95)

        backup()
        self.backups_list()

        removedir(pth+dtm+s+'bin')
        removedir(pth+dtm+s+'mods')

        self.mc_progressBar.setProperty("value", 97)

        move(pth+'.mpm_cache'+s+'minecraft'+s+'temp'+s+'.minecraft'+s+'bin',
            pth+dtm+s+'bin')
        
        removedir(pth+'.mpm_cache'+s+'minecraft'+s+'temp')

        set_status += ok+QtGui.QApplication.translate("progress_install_minecraft", "Unpacking the minecraft.jar", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser.setHtml(set_status)
        main_window.unpacking_client()

        config = ConfigParser.ConfigParser()
        config.read(pth+'.mpm_cache'+s+'config.ini')
        if (config.has_section(dtm)==False):
            config.add_section(dtm)
        config.set(dtm, 'version', select_version)
        
        with open(pth+'.mpm_cache'+s+'config.ini', 'wb') as configfile:
            config.write(configfile)

        set_status += ok+QtGui.QApplication.translate("progress_install_minecraft", "Download lists of mods", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser.setHtml(set_status)
        self.mc_progressBar.setProperty("value", 98)

        global minecraft_version
        minecraft_version = select_version

        main_window.initializing()

        set_status += ok+QtGui.QApplication.translate("progress_install_minecraft", "Install completed", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser.setHtml(set_status)
        self.mc_progressBar.setProperty("value", 100)
    ############################################################################
    # </ install minecraft >
    ############################################################################



    ############################################################################
    # < install mods >
    ############################################################################
    def loadProgressMods(self, bl, blsize, size):
        p = float(min(bl*blsize, size)) / size * 85 + 5
        self.mc_progressBar.setProperty("value", p)
        self.mod_progressBar.setProperty("value", p)
        QtCore.QCoreApplication.processEvents()
        
    def select_mod(self):
        for item in self.listWidget_2.selectedItems():
            select_mod = str(item.text())

        self.mod_progressBar.show()
        self.mod_install.hide()
        self.mod_enable.hide()
        self.mod_disable.hide()
        self.mod_remove.hide()

        mod_info = ConfigParser.ConfigParser()
        mod_info.read(pth+'.mpm_cache'+s+mods_list_file)
        depending = mod_info.get(select_mod, 'depending')

        set_status = QtGui.QApplication.translate("progress_install_mods", "Pre-installation preparation", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser_2.setHtml(QtGui.QApplication.translate(
            "DownloadWindow", set_status,
            None, QtGui.QApplication.UnicodeUTF8))

        if (depending != ''):
            depending = depending.split(', ')
            for dependence in depending:
                print 'At the first install the mod '+dependence
                set_status = QtGui.QApplication.translate("progress_install_mods", "At the first install the mod", None, QtGui.QApplication.UnicodeUTF8)+' '+dependence
                self.textBrowser_2.append(QtGui.QApplication.translate(
                    "DownloadWindow", set_status,
                    None, QtGui.QApplication.UnicodeUTF8))
                self.install_mod(dependence)
        self.install_mod(select_mod)


    def install_mod(self, select_mod):
        mod_info = ConfigParser.ConfigParser()
        mod_info.read(pth+'.mpm_cache'+s+mods_list_file)
        url = mod_info.get(select_mod, 'url')
        filename = mod_info.get(select_mod, 'filename')
        md5 = mod_info.get(select_mod, 'md5')

        self.mod_progressBar.setProperty("value", 1)
        set_status = QtGui.QApplication.translate("progress_install_mods", "Download mod", None, QtGui.QApplication.UnicodeUTF8)+' <b>'+select_mod+'</b>'
        self.textBrowser_2.append(QtGui.QApplication.translate(
            "DownloadWindow", set_status,
            None, QtGui.QApplication.UnicodeUTF8))
        

        ptf = pth+'.mpm_cache'+s+'mods'+s+filename
        if (os.path.exists(ptf)==False or md5hash(ptf)!=md5):
            urllib.urlretrieve(url, ptf, self.loadProgressMods)

        removedir(pth+'.mpm_cache'+s+'mods'+s+'temp')
        mkdir(pth+'.mpm_cache'+s+'mods'+s+'temp')

        if (mod_info.has_option(select_mod, 'in_mods') and mod_info.get(select_mod, 'in_mods')=='<THIS>'):
            pass
        else:
            set_status = QtGui.QApplication.translate("progress_install_mods", "Unpacking", None, QtGui.QApplication.UnicodeUTF8)
            self.textBrowser_2.append(QtGui.QApplication.translate(
                "DownloadWindow", set_status, None,
                QtGui.QApplication.UnicodeUTF8))

            self.mod_progressBar.setProperty("value", 91)

            qazwsx = filename.split('.')

            if (os.name=='posix'):
                if (qazwsx[1]=='zip'):
                    term('cd '+pth+'.mpm_cache/mods/ && unzip '+filename+' -d temp')
                elif (qazwsx[1]=='7z'):
                    term('cd '+pth+'.mpm_cache/mods/ && 7z x '+filename+' -otemp')
                else:
                    print qazwsx[1]+' - unknown format'
            elif (os.name=='nt'):
                term('bin\\7za.exe x "'+pth+'.mpm_cache\\mods\\'+filename+'" -o"'+pth+'.mpm_cache\\mods\\temp"')

        self.mod_progressBar.setProperty("value", 95)

        set_status = QtGui.QApplication.translate("progress_install_mods", "Install", None, QtGui.QApplication.UnicodeUTF8)
        self.textBrowser_2.append(QtGui.QApplication.translate(
            "DownloadWindow", set_status, None,
            QtGui.QApplication.UnicodeUTF8))

        if (mod_info.has_option(select_mod, 'in_jar')):
            if (os.name=='posix'):
                term('cp -R '+pth+'.mpm_cache'+s+'mods'+s+'temp'+s+'"'+mod_info.get(select_mod, 'in_jar')+'"* '+pth+dtm+s+'bin'+s+'minecraft.jar'+s)
            elif (os.name=='nt'):
                in_jar = mod_info.get(select_mod, 'in_jar').replace('/', '\\')
                term('xcopy /E /Y "'+pth+'.mpm_cache\\mods\\temp\\'+in_jar+'"* "'+pth+dtm+s+'bin'+'\\"minecraft.jar')

            removedir(pth+dtm+s+'bin'+s+'minecraft_jar'+s+'META-INF')

        if (mod_info.has_option(select_mod, 'in_mods')):
            mkdir(pth+dtm+s+'mods')

            if (mod_info.get(select_mod, 'in_mods')=='<THIS>'):
                if (os.name=='posix'):
                    term('cp -R '+pth+'.mpm_cache'+s+'mods'+s+filename+' '+pth+dtm+s+'mods'+s)
                elif (os.name=='nt'):
                    in_mods = filename.replace('/', '\\')
                    term('xcopy /E /Y "'+pth+'.mpm_cache\\mods\\'+in_mods+'"* "'+pth+dtm+s+'"mods')
            else:
                if (os.name=='posix'):
                    term('cp -R '+pth+'.mpm_cache'+s+'mods'+s+'temp'+s+'"'+mod_info.get(select_mod, 'in_mods')+'"* '+pth+dtm+s+'mods'+s)
                elif (os.name=='nt'):
                    in_mods = mod_info.get(select_mod, 'in_mods').replace('/', '\\')
                    term('xcopy /E /Y "'+pth+'.mpm_cache\\mods\\temp\\'+in_mods+'"* "'+pth+dtm+s+'"mods')

        if (mod_info.has_option(select_mod, 'in_minecraft')):
            in_mc = mod_info.get(select_mod, 'in_minecraft').split(';')
            if (os.name=='posix'):
                term('cp -R '+pth+'.mpm_cache'+s+'mods'+s+'temp'+s+'"'+in_mc[0]+s+'"* "'+pth+dtm+s+in_mc[1]+s+'"')
            elif (os.name=='nt'):
                in_mc = in_mc.replace('/', '\\')
                term('xcopy /E /Y "'+pth+'.mpm_cache\\mods\\temp\\'+in_mc[0]+'" "'+pth+dtm+s+in_mc[1]+s+'"')

        removedir(pth+'.mpm_cache'+s+'mods'+s+'temp')

        main_window.updateListMods()

        set_status = '<span style="color: green;">'+QtGui.QApplication.translate("progress_install_mods", "Install completed", None, QtGui.QApplication.UnicodeUTF8)+'</span>'
        self.textBrowser_2.append(QtGui.QApplication.translate(
            "DownloadWindow", set_status,
            None, QtGui.QApplication.UnicodeUTF8))
        self.mod_progressBar.setProperty("value", 100)
    ############################################################################
    # </ install mods >
    ############################################################################



    ############################################################################
    # < backups >
    ############################################################################
    def backup_restore(self):
        for item in self.listWidget_3.selectedItems():
            select_version = str(item.text())
        print select_version

        removedir(pth+dtm+s+'bin')
        removedir(pth+dtm+s+'mods')

        move(pth+dtm+s+'backup'+s+select_version+s+'bin',
            pth+dtm+s+'bin')
        move(pth+dtm+s+'backup'+s+select_version+s+'mods',
            pth+dtm+s+'mods')

        try:
            select_version.split(' - ')

            config = ConfigParser.ConfigParser()
            config.read(pth+'.mpm_cache'+s+'config.ini')
            if (config.has_section(dtm)==False):
                config.add_section(dtm)
            config.set(dtm, 'version', select_version[0])
            
            with open(pth+'.mpm_cache'+s+'config.ini', 'wb') as configfile:
                config.write(configfile)
        except:
            pass

        removedir(pth+dtm+s+'backup'+s+select_version)
        self.backups_list()

    def backup_remove(self):
        for item in self.listWidget_3.selectedItems():
            select_version = str(item.text())
        try:
            removedir(pth+dtm+s+'backup'+s+select_version)
            self.backups_list()
        except:
            pass

    def backup_create(self):
        if os.path.exists(pth+dtm+s+'bin'):
            backup()
            QtGui.QMessageBox.information(QtGui.QWidget(),
                QtGui.QApplication.translate("backup_create", "Ready", None, QtGui.QApplication.UnicodeUTF8),
                QtGui.QApplication.translate("backup_create", "Backup created successfully", None, QtGui.QApplication.UnicodeUTF8),
                QtGui.QMessageBox.Ok)
            self.backups_list()
        else:
            QtGui.QMessageBox.critical(QtGui.QWidget(),
                QtGui.QApplication.translate("backup_create", "Error", None, QtGui.QApplication.UnicodeUTF8),
                QtGui.QApplication.translate("backup_create", "Cannot perform this action<br>because the minecraft is not installed", None, QtGui.QApplication.UnicodeUTF8),
                QtGui.QMessageBox.Ok)        
    ################################################################################
    # </ backups >
    ################################################################################





################################################################################
# < about >
################################################################################
class About(QtGui.QMainWindow, Ui_about):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon(ich+"icons/icon_16.png"))
        self.icon.setPixmap(QtGui.QPixmap(ich+"/icons/icon_130.png"))
        fl = open(ich+"gpl-2.0.txt", "r")
        self.textEdit_3.setText(fl.read())
        self.pushButton.clicked.connect(self.close)
################################################################################
# </ about >
################################################################################







################################################################################
# < greeting of user, if minecraft is already set >
################################################################################
class User_hello(QtGui.QMainWindow, Ui_user_hello):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.setWindowIcon(QtGui.QIcon(ich+"icons/icon_16.png"))

        menecraft_list_package = ConfigParser.ConfigParser()
        menecraft_list_package.read(pth+'.mpm_cache'+s+'menecraft_info_package.ini')
        f = menecraft_list_package.sections()
        i=0
        for line in f:
            self.comboBox.addItem("")
            self.comboBox.setItemText(i,
                QtGui.QApplication.translate(
                "user_hello", line,
                None, QtGui.QApplication.UnicodeUTF8))
            i+=1

        self.pushButton.clicked.connect(self.set_minecraft_version)
        self.pushButton_2.clicked.connect(self.weiter)

    def set_minecraft_version(self):
        minecraft_version = str(self.comboBox.currentText())

        conftemp = ConfigParser.ConfigParser()
        conftemp.read(pth+'.mpm_cache'+s+'config.ini')

        if (conftemp.has_section(dtm)==False):
            conftemp.add_section(dtm)

        conftemp.set(dtm, 'version', minecraft_version)

        with open(pth+'.mpm_cache'+s+'config.ini', 'wb') as configfile:
            conftemp.write(configfile)

        global mods_list_file
        mods_list_file = 'mods_'+minecraft_version+'.ini'
        urllib.urlretrieve(default_server+mods_list_file,
            filename=pth+'.mpm_cache'+s+mods_list_file)

        main_window.initializing()

        self.close()

    def weiter(self):
        self.close()
################################################################################
# </ greeting of user, if minecraft is already set >
################################################################################



################################################################################
# < md5 hash >
################################################################################
def md5hash(filePath):
     file = open(filePath, 'rb')
     m = hashlib.md5()
     while True:
         data = file.read(8000)
         if not data:
             break
         m.update(data)
     return m.hexdigest()
################################################################################
# </ md5 hash >
################################################################################


################################################################################
################################################################################
################################################################################
def backup():
    if os.path.exists(pth+dtm+s+'bin')==True:
        mkdir(pth+dtm+s+'backup')
        # delete milliseconds
        backup_name = str(datetime.now())[:-7]

        if (os.name=='nt'):
            backup_name = backup_name.replace(':', '.')

        config = ConfigParser.ConfigParser()
        config.read(pth+'.mpm_cache'+s+'config.ini')
        if (config.has_option(dtm, 'version')==True):
            minecraft_version = config.get(dtm, 'version')
        else:
            minecraft_version = 'unknown'

        folder_name = minecraft_version+' - '+backup_name

        shutil.copytree(pth+dtm+s+'bin', pth+dtm+s+'backup'+s+folder_name+s+'bin')
        if os.path.exists(pth+dtm+s+'mods')==True:
            shutil.copytree(pth+dtm+s+'mods', pth+dtm+s+'backup'+s+folder_name+s+'mods')

def remove(patch):
    if os.path.exists(patch)==True:
        if (os.name=='posix'):
            os.system('rm "'+patch+'"')
        elif (os.name=='nt'):
            os.system('del /S /Q "'+patch.encode('cp1251')+'"')

def removedir(patch):
    if os.path.exists(patch)==True:
        if (os.name=='posix'):
            os.system('rm -R "'+patch+'"')
        elif (os.name=='nt'):
            os.system('rmdir /S /Q "'+patch.encode('cp1251')+'"')
################################################################################
################################################################################
################################################################################

config = ConfigParser.ConfigParser()
config.read(pth+'.mpm_cache'+s+'config.ini')

if (config.has_option('mpm', 'locale')==True):
    list_locale = config.get('mpm', 'locale')
else:
    list_locale = 'default'

if list_locale=='default':
    locale = QtCore.QLocale.system().name()[:5]
elif list_locale=='STANDARD':
    locale = 'STANDARD'
else:
    if (os.path.isdir(ich+s+'locals')):
        for line in os.listdir(ich+'locals'):
            if (line[-3:]=='.qm' and list_locale == line[:-3]):
                locale = list_locale

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    if (locale!='STANDARD' and os.path.exists(ich+s+'locals'+s+locale+'.qm')):
        translator = QtCore.QTranslator(app)
        translator.load(ich+s+'locals'+s+locale+'.qm')
        app.installTranslator(translator)

    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())