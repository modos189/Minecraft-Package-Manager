<?xml version="1.0" ?><!DOCTYPE TS><TS language="ru_RU" version="2.0">
<context>
    <name>Initializing</name>
    <message>
        <location filename="MPM.py" line="421"/>
        <source>You must install Minecraft of MPM</source>
        <translation>Необходимо установить Minecraft из MPM</translation>
    </message>
    <message>
        <location filename="MPM.py" line="448"/>
        <source>Download list package...</source>
        <translation>Загрузка списка пакетов...</translation>
    </message>
    <message>
        <location filename="MPM.py" line="456"/>
        <source>Currently installed version of minecraft</source>
        <translation>Сейчас установлен Minecraft версии</translation>
    </message>
</context>
<context>
    <name>Languages</name>
    <message>
        <location filename="MPM.py" line="307"/>
        <source>Use system default</source>
        <translation>Использовать параметры системы</translation>
    </message>
    <message>
        <location filename="MPM.py" line="308"/>
        <source>Original</source>
        <translation>Исходный текст</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.py" line="231"/>
        <source>Minecraft Package Manager</source>
        <translation>Minecraft Package Manager</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="232"/>
        <source>Add the file to the database</source>
        <translation>Добавить файл в базу данных</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="236"/>
        <source>Install</source>
        <translation>Установить</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="240"/>
        <source>More</source>
        <translation>Подробнее</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="235"/>
        <source>Minecraft</source>
        <translation>Minecraft</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="244"/>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="238"/>
        <source>Disable</source>
        <translation>Выключить</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="239"/>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="241"/>
        <source>Mods</source>
        <translation>Модификации</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="242"/>
        <source>Restore</source>
        <translation>Восстановить</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="243"/>
        <source>Create backup</source>
        <translation>Создать резервную копию</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="245"/>
        <source>Backups</source>
        <translation>Резервные копии</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="246"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="250"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="248"/>
        <source>This feature provides a way to obtain files from third-party servers</source>
        <translation>Эта функция позволяет получать файлы со сторонных серверов</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="249"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="251"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="252"/>
        <source>toolBar</source>
        <translation>toolBar</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="253"/>
        <source>Start game</source>
        <translation>Начать игру</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="254"/>
        <source>Copy Launcher on your desktop</source>
        <translation>Скопировать лаунчер на рабочий стол</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="255"/>
        <source>About MPM</source>
        <translation>О MPM</translation>
    </message>
    <message>
        <location filename="MainWindow.py" line="256"/>
        <source>Donate</source>
        <translation>Пожертвования</translation>
    </message>
    <message>
        <location filename="MPM.py" line="142"/>
        <source>Software update available: v</source>
        <translation>Доступно обновление приложения: v</translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="about.py" line="79"/>
        <source>About me</source>
        <translation>О MPM</translation>
    </message>
    <message>
        <location filename="about.py" line="80"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot;  font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot;  font-weight:600;&quot;&gt;Minecraft Package Manager&lt;/span&gt;&lt;span style=&quot; &quot;&gt; - this program allows&lt;br /&gt;you to download and automatically install the&lt;br /&gt;required version of Minecraft. Also there is the&lt;br /&gt;possibility to download and automatically install&lt;br /&gt;various mods and its dependencies.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;You can visit:&lt;br /&gt;Official page (eng): &lt;/span&gt;&lt;span style=&quot;  font-style:italic;&quot;&gt;http://mpm.modostroi.ru&lt;/span&gt;&lt;span style=&quot; &quot;&gt;&lt;br /&gt;Frequently asked questions list: &lt;/span&gt;&lt;span style=&quot;  font-style:italic;&quot;&gt;https://github.com/modos189/Minecraft-Package-Manager/wiki/FAQ-(eng)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Minecraft Package Manager&lt;/span&gt; - это простая программа с широким функционалом для быстрой и лёгкой установки популярной игры Minecraft и модификаций для неё.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Вы можете посетить:&lt;br /&gt;Официальная страница (rus): &lt;span style=&quot; font-style:italic;&quot;&gt;http://mpm.modostroi.ru&lt;/span&gt;&lt;br /&gt;FAQ (rus): &lt;span style=&quot; font-style:italic;&quot;&gt;https://github.com/modos189/Minecraft-Package-Manager/wiki/FAQ-(rus)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="about.py" line="86"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="about.py" line="87"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot;  font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;Thanks to the guys from &lt;/span&gt;&lt;span style=&quot;  font-style:italic;&quot;&gt;python.su&lt;/span&gt;&lt;span style=&quot; &quot;&gt; for the help.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; &quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;Special thanks to:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;Kirichenko Dmitry&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;Goma Anatoliy&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;Romanov Nikolay&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; &quot;&gt;Skorobogatyy Alexey&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Спасибо ребятам с python.su за помощь.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Отдельное спасибо этим людям:&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Кириченко Дмитрий и Гома Анатолий за многочисленные тестирования и идеи,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Романов Николай за упаковку в .deb,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Скоробогатый Алексей за ручку с логотипом яндекса =) и определения пути развития.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="about.py" line="98"/>
        <source>Thanks</source>
        <translation>Спасибо</translation>
    </message>
    <message>
        <location filename="about.py" line="99"/>
        <source>GPL</source>
        <translation>GPL</translation>
    </message>
    <message>
        <location filename="about.py" line="100"/>
        <source>About MPM</source>
        <translation>О MPM</translation>
    </message>
    <message>
        <location filename="about.py" line="101"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>load lists</name>
    <message>
        <location filename="MPM.py" line="483"/>
        <source>list package minecraft not available</source>
        <translation>Список пакетов minecraft не доступен</translation>
    </message>
    <message>
        <location filename="MPM.py" line="491"/>
        <source>list package minecraft damaged</source>
        <translation>Список пакетов minecraft повреждён</translation>
    </message>
    <message>
        <location filename="MPM.py" line="499"/>
        <source>list package mods not available</source>
        <translation>Список пакетов модов не доступен</translation>
    </message>
    <message>
        <location filename="MPM.py" line="507"/>
        <source>list package mods damaged</source>
        <translation>Список пакетов модов повреждён</translation>
    </message>
</context>
<context>
    <name>mod_desc</name>
    <message>
        <location filename="MPM.py" line="569"/>
        <source>no</source>
        <translation>нет</translation>
    </message>
    <message>
        <location filename="MPM.py" line="574"/>
        <source>Depending:</source>
        <translation>Зависимости:</translation>
    </message>
    <message>
        <location filename="MPM.py" line="597"/>
        <source>Disable</source>
        <translation>Выключить</translation>
    </message>
    <message>
        <location filename="MPM.py" line="605"/>
        <source>Enable</source>
        <translation>Включить</translation>
    </message>
</context>
<context>
    <name>progress_install_minecraft</name>
    <message>
        <location filename="MPM.py" line="643"/>
        <source>ready</source>
        <translation>готово</translation>
    </message>
    <message>
        <location filename="MPM.py" line="644"/>
        <source>Pre-installation preparation</source>
        <translation>Предустановочная подготовка</translation>
    </message>
    <message>
        <location filename="MPM.py" line="648"/>
        <source>Download client</source>
        <translation>Загрузка клиента</translation>
    </message>
    <message>
        <location filename="MPM.py" line="657"/>
        <source>Unpacking the archive</source>
        <translation>Распаковка архива</translation>
    </message>
    <message>
        <location filename="MPM.py" line="696"/>
        <source>Unpacking the minecraft.jar</source>
        <translation>Распаковка minecraft.jar</translation>
    </message>
    <message>
        <location filename="MPM.py" line="709"/>
        <source>Download lists of mods</source>
        <translation>Загрузка списка модов</translation>
    </message>
    <message>
        <location filename="MPM.py" line="718"/>
        <source>Install completed</source>
        <translation>Установка завершена</translation>
    </message>
</context>
<context>
    <name>progress_install_mods</name>
    <message>
        <location filename="MPM.py" line="744"/>
        <source>Pre-installation preparation</source>
        <translation>Предустановочная подготовка</translation>
    </message>
    <message>
        <location filename="MPM.py" line="753"/>
        <source>At the first install the mod</source>
        <translation>Сначала установка мода</translation>
    </message>
    <message>
        <location filename="MPM.py" line="769"/>
        <source>Download mod</source>
        <translation>Загрузка мода</translation>
    </message>
    <message>
        <location filename="MPM.py" line="785"/>
        <source>Unpacking</source>
        <translation>Распаковка</translation>
    </message>
    <message>
        <location filename="MPM.py" line="808"/>
        <source>Install</source>
        <translation>Установка</translation>
    </message>
    <message>
        <location filename="MPM.py" line="855"/>
        <source>Install completed</source>
        <translation>Установка завершена</translation>
    </message>
</context>
<context>
    <name>user_hello</name>
    <message>
        <location filename="user_hello.py" line="57"/>
        <source>Warning!</source>
        <translation>Внимание!</translation>
    </message>
    <message>
        <location filename="user_hello.py" line="58"/>
        <source>You have already installed the minecraft. Select an action:</source>
        <translation>У Вас Minecraft уже установлен. Выберите действие:</translation>
    </message>
    <message>
        <location filename="user_hello.py" line="59"/>
        <source>1. Select which version&lt;br&gt;of the game is installed:</source>
        <translation>1. Выберите, какая версия&lt;br&gt;игры установлена:</translation>
    </message>
    <message>
        <location filename="user_hello.py" line="60"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="user_hello.py" line="61"/>
        <source>2. Continue and install another&lt;br&gt;version of the game by using MPM</source>
        <translation>2. Продолжить и установить другую&lt;br&gt;версию игры с помощью MPM</translation>
    </message>
    <message>
        <location filename="user_hello.py" line="62"/>
        <source>Continue</source>
        <translation>Продолжить</translation>
    </message>
</context>
</TS>